package br.ucsal.bes20211.poo.aula10;

public class ExemploTUI {

	public static void main(String[] args) {

		testarContaCorrente();
		System.out.println("coisas que podem ocorrer se tudo der certo!");

	}

	private static void testarContaCorrente() {

		System.out.println("########### Conta Corrente ###########");
		ContaCorrente contaCorrente1 = new ContaCorrente("Claudio Neiva");
		System.out.println("Número: " + contaCorrente1.getNumero());
		System.out.println("Correntista: " + contaCorrente1.getNomeCorrentista());
		contaCorrente1.depositar(45000d);
		sacar(contaCorrente1, 40d);
		sacar(contaCorrente1, 120d);
		System.out.println("Saldo: " + contaCorrente1.consultarSaldo());

		System.out.println("\n\n########### Conta Corrente ###########");
		ContaCorrente contaCorrente2 = new ContaCorrente("Deputado 1");
		System.out.println("Número: " + contaCorrente2.getNumero());
		System.out.println("Correntista: " + contaCorrente2.getNomeCorrentista());
		contaCorrente2.depositar(12000d);
		sacar(contaCorrente2, 30d);
		sacar(contaCorrente2, 80d);
		sacar(contaCorrente2, 23000d);
		sacar(contaCorrente2, 3330d);
		System.out.println("Saldo: " + contaCorrente2.consultarSaldo());
	}

	private static void sacar(ContaCorrente contaCorrente, Double valor) {
		try {
			contaCorrente.sacar(valor);
		} catch (NegocioException e) {
			System.out.println("Problema ao realizar o saque: " + e.getMessage());
		}
	}

}
