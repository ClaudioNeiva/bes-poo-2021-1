package br.ucsal.bes20211.poo.aula10;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NegocioException(String message) {
		super(message);
	}

}
