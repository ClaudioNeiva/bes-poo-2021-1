package br.ucsal.bes20211.poo.aula10;

/*
 * 1 -> constantes
 * 
 * 2 -> atributos
 * 
 * 3 -> construtores
 * 
 * 4 -> métodos públicos
 * 
 * 4.1 -> serviços específicos / métodos de negócio 
 * 
 * 4.1.1 -> métodos privados  **
 * 
 * 4.2 -> getters/setters
 * 
 * 4.3 -> hashcode / equals
 * 
 * 4.4 -> toString
 * 
 * 5 -> métodos privados  **
 * 
 */

public class ContaCorrente {

	private static Integer cont = 0;

	private Integer numero;

	private String nomeCorrentista;

	private Double saldo;

	public ContaCorrente(String nomeCorrentista) {
		super();
		this.nomeCorrentista = nomeCorrentista;
		definirNumero();
		this.saldo = 0d;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	// Exception = exceção checked!
	// RuntimeException = exceção unchecked!

	public Double sacar(Double valor) throws NegocioException {
		if (valor > saldo) {
			throw new NegocioException("Saldo insuficiente para saque de " + valor + ".");
		}
		saldo -= valor;
		return saldo;
	}

	public Double consultarSaldo() {
		return saldo;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
	}

	public Integer getNumero() {
		return numero;
	}

	private void definirNumero() {
		ContaCorrente.cont++;
		numero = cont;
	}

}
