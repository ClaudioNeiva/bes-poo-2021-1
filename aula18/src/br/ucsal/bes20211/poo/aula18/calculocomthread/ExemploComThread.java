package br.ucsal.bes20211.poo.aula18.calculocomthread;

/*
 * exibirPrimos(100000, 200000)

com a thread principal:
	
	duração=36837
	
	duração=37020
	
com 2 threads:
	
	duração=21578
	
	duração=21589
	
com 3 threads:
	
	duração=15721	
	
	duração=16004
	
com 4 threads:

	duração=13373
	
	duração=13287
	
com 5 threads:

	duração=12301
	
	duração=12861
 */

public class ExemploComThread {

	public static void main(String[] args) throws InterruptedException {
		long timeInicial = System.currentTimeMillis();

		Calculo calculo1 = new Calculo(100000, 120000);
		Thread thread1 = new Thread(calculo1);

		Calculo calculo2 = new Calculo(120001, 140000);
		Thread thread2 = new Thread(calculo2);

		Calculo calculo3 = new Calculo(140001, 160000);
		Thread thread3 = new Thread(calculo3);

		Calculo calculo4 = new Calculo(160001, 180000);
		Thread thread4 = new Thread(calculo4);

		Calculo calculo5 = new Calculo(180001, 200000);
		Thread thread5 = new Thread(calculo5);

		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();

		thread1.join();
		thread2.join();
		thread3.join();
		thread4.join();
		thread5.join();

		long timeFinal = System.currentTimeMillis();
		long duracao = timeFinal - timeInicial;
		System.out.println("duração=" + duracao);
	}

}
