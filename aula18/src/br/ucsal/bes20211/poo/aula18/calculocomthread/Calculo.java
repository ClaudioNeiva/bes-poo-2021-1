package br.ucsal.bes20211.poo.aula18.calculocomthread;

// Não é interessante ser subclasse de thread, porque isso impede de ser
// subclasse de outra classe.
//public class Calculo extends Thread{
public class Calculo implements Runnable {

	private int min;
	private int max;

	public Calculo(int min, int max) {
		super();
		this.min = min;
		this.max = max;
	}

	@Override
	public void run() {
		exibirPrimos(min, max);
	}

	private static void exibirPrimos(int min, int max) {
		for (int i = min; i <= max; i++) {
			exibirPrimo(i);
		}
	}

	private static void exibirPrimo(int n) {
		if (qtdDivisores(n) == 2) {
			System.out.println(n);
		}
	}

	private static int qtdDivisores(int n) {
		int qtd = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				qtd++;
			}
		}
		return qtd;
	}

}
