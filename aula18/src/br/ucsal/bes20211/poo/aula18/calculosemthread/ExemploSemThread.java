package br.ucsal.bes20211.poo.aula18.calculosemthread;

public class ExemploSemThread {

	public static void main(String[] args) {
		long timeInicial = System.currentTimeMillis();
		exibirPrimos(100000, 200000);
		long timeFinal = System.currentTimeMillis();
		long duracao = timeFinal - timeInicial;
		System.out.println("duração=" + duracao);
	}

	private static void exibirPrimos(int min, int max) {
		for (int i = min; i <= max; i++) {
			exibirPrimo(i);
		}
	}

	private static void exibirPrimo(int n) {
		if (qtdDivisores(n) == 2) {
			System.out.println(n);
		}
	}

	private static int qtdDivisores(int n) {
		int qtd = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				qtd++;
			}
		}
		return qtd;
	}

}
