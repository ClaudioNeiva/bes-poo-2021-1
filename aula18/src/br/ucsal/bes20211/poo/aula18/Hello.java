package br.ucsal.bes20211.poo.aula18;

import java.text.MessageFormat;

public class Hello {

	public static void main(String[] args) {

		Pessoa pessoa1 = new Pessoa("Claudio", "123");

		System.out.println("Bom dia, " + pessoa1.getNome() + "!");

//		String nome = "claudio"
//		
//		`Bom dia, {nome}!`
//		
//		String.format("Bom dia, {0}!", nome);
//		
		System.out.printf("A nota é %10.2f, para o aluno %s.\n",20.,"claudio neiva");

		System.out.println(MessageFormat.format("Bom dia {0}, você é o {1} aluno. Seu nome é {0}.", "Claudio",10));
		
	}

}
