package br.ucsal.bes20211.poo.aula02;

public class Exemplo {

	public static void main(String[] args) {
		
		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		Aluno aluno3 = aluno1;
		
		aluno1.nome = "Claudio Neiva";
		aluno2.nome = "Davi Lima";
		aluno3.nome = "Robert Matos";
		
		System.out.println(aluno1.nome);
		System.out.println(aluno2.nome);
		System.out.println(aluno3.nome);

		System.out.println(aluno1);
		System.out.println(aluno2);
		System.out.println(aluno3);

	}
	
}
