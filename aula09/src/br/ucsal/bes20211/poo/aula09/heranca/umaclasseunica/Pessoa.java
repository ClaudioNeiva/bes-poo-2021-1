
package br.ucsal.bes20211.poo.aula09.heranca.umaclasseunica;

import java.time.LocalDate;
import java.util.List;

import br.ucsal.bes20211.poo.aula09.heranca.cadaclassecomtudodentro.Endereco;

//NÃO É BOM!
public class Pessoa {

	TipoPessoaEnum tipoPessoa;

	String cpf;

	String nome;

	LocalDate dataNascimento;

	Endereco endereco;

	List<String> telefones;

	String nomeMae;

	String cnpj;

	Integer inscricaoEstadual;

	Integer inscricaoMunicipal;

	// CNAE = Classificação Nacional de Atividades Econômicas
	List<String> cnaes;

}
