package br.ucsal.bes20211.poo.aula09.heranca.hierarquia;

import java.time.LocalDate;
import java.util.Arrays;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica1 = new PessoaFisica();

		pessoaFisica1.cpf = "2134132";
		pessoaFisica1.nome = "Claudio Neiva";
		pessoaFisica1.dataNascimento = LocalDate.of(2000, 5, 10);
		pessoaFisica1.endereco = "Rua X";
		pessoaFisica1.email = "claudio@ucsal";

		pessoaFisica1.telefones = Arrays.asList("12312", "4564564");
		// Utilizar o Arrays.asList terá o mesmo efeito das linhas abaixo:
		// pessoaFisica1.telefones = new ArrayList<>();
		// pessoaFisica1.telefones.add("12312");
		// pessoaFisica1.telefones.add("4564564");

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica();
		pessoaJuridica1.cnpj = "345634534534";
		pessoaJuridica1.nome = "Casas Bahia";
		pessoaJuridica1.inscricaoEstadual = 234;
		pessoaJuridica1.inscricaoMunicipal = 567865;
		pessoaJuridica1.telefones = Arrays.asList("34563465", "3243434344", "2345345345");
		pessoaJuridica1.endereco = "Rua Y";
		pessoaJuridica1.email = "contato@casasbahia.com.br";

		// up cast: passando a subclasse onde é esperada uma superclasse.
		exibirDadosBasicos(pessoaFisica1);
		
		System.out.println("-----------------------------");
		
		// up cast: passando a subclasse onde é esperada uma superclasse.
		exibirDadosBasicos(pessoaJuridica1);

	}

	private static void exibirDadosBasicos(Pessoa pessoa) {
		System.out.println("Nome:" + pessoa.nome);
		System.out.println("Endereco:" + pessoa.endereco);
		System.out.println("Telefones:" + pessoa.telefones);
		System.out.println("Email:" + pessoa.email);
	}

}
