package br.ucsal.bes20211.poo.aula09.revisao.enumeracoes;

import java.util.Scanner;

public class ExemploEnumeracoes {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		TipoComunicacaoEnum tipoComunicacao;
		String tipoComunicacaoString;
		
		System.out.println("Informe o tipo de comunicação desejado:");
		tipoComunicacaoString = scanner.nextLine();
		tipoComunicacao = TipoComunicacaoEnum.valueOf(tipoComunicacaoString.toUpperCase());
		
		ComunicacaoUtil.estabelecerComunicacao(tipoComunicacao);
	}

}
