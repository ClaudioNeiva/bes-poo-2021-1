package br.ucsal.bes20211.poo.aula09.revisao.listas;

import java.util.ArrayList;
import java.util.List;

public class ExemploList {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		// nomes.add(LocalDate.of(2000, 5, 30));
		// nomes.add(new Aluno("claudi", 2000, "claudio@ucasl", "Rua x");

		nomes.add("Claudio");
		nomes.add("Antonio");
		nomes.add("Pedreira");
		nomes.add("asddasdasd");

		System.out.println("Primeiro nomes=" + nomes.get(0));
		System.out.println("Segundo nomes=" + nomes.get(1));

		System.out.println("Tamanho da lista=" + nomes.size());

		nomes.remove(2);

		System.out.println("nomes=" + nomes);

		nomes.clear();
		System.out.println("Tamanho da lista=" + nomes.size());
		
	}

}
