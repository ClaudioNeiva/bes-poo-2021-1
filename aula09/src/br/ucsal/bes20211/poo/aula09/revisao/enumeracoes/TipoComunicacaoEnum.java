package br.ucsal.bes20211.poo.aula09.revisao.enumeracoes;

public enum TipoComunicacaoEnum {

	EMAIL("contato@ucsal.br"),

	CHAT("Assistente Virtual UCSal"),

	TELEFONE("(71)2333-2323"),

	FAX("(71)2333-77565"),

	BIP("32323");

	private String contato;

	private TipoComunicacaoEnum(String contato) {
		this.contato = contato;
	}

	public String getContato() {
		return contato;
	}

}
