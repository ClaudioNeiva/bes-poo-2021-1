package br.ucsal.bes20211.poo.aula09.revisao.listas;

public class Aluno {

	private static Integer seq = 0;

	private Integer matricula;

	private String nome;

	private Integer anoNascimento;

	private String email;

	private String endereco;

	public Aluno(String nome, Integer anoNascimento, String email, String endereco) {
	
		Aluno.seq++;
		this.matricula = Aluno.seq;

		this.nome = nome;
		this.anoNascimento = anoNascimento;
		this.email = email;
		this.endereco = endereco;
	}

	public static Integer getSeq() {
		return Aluno.seq;
	}

	// Métodos de instância podem acessar atributos estáticos!  
	public Integer metodoInstancia() {
		return Aluno.seq;
	}

	// Métodos estático NÃO podem acessar atributos de instância!  
	public static Integer metodoEstatico() {
		// return this.matricula; NÃO COMPILA!
		// return matricula;  NÃO COMPILA!
		// "return this.matricula" e "return matricula" é EXATAMENTE a mesma coisa!
		return Aluno.seq;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", anoNascimento=" + anoNascimento + ", email="
				+ email + ", endereco=" + endereco + "]";
	}

}
