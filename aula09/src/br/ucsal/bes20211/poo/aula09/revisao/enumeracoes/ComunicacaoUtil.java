package br.ucsal.bes20211.poo.aula09.revisao.enumeracoes;

public class ComunicacaoUtil {

	public static void estabelecerComunicacao(TipoComunicacaoEnum tipoComunicacao) {
		if (TipoComunicacaoEnum.EMAIL.equals(tipoComunicacao)) {
			System.out.println("Vamos enviar uma email, com remetente " + tipoComunicacao.getContato() + "...");
			// escrever um código para passar um email.
		} else if (TipoComunicacaoEnum.CHAT.equals(tipoComunicacao)) {
			System.out.println("Vamos enviar uma mensagem de chat, com pessoa " + tipoComunicacao.getContato() + "...");
			// escrever um código para enviar uma mensagem no chat.
		}
	}

}
