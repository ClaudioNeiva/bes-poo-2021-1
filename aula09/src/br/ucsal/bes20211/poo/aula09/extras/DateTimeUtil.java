package br.ucsal.bes20211.poo.aula09.extras;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public static LocalDate string2LocalDate(String dataString) {
		return LocalDate.parse(dataString, dtf);
	}
	
}
