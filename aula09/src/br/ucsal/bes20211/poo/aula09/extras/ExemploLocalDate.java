package br.ucsal.bes20211.poo.aula09.extras;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ExemploLocalDate {

	private static Scanner scanner = new Scanner(System.in);

	private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public static void main(String[] args) {
		String dataString;
		LocalDate data;

		System.out.println("Informe a data (dd/mm/aaaa):");
		dataString = scanner.nextLine();

		data = LocalDate.parse(dataString, dtf);

		data = DateTimeUtil.string2LocalDate(dataString);

		System.out.println("Data (formato original):" + data);
		System.out.println("Data (formatada):" + data.format(dtf));
	}
}
