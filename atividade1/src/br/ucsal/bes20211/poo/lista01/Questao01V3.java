package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Questao01V3 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(nota, conceito);
	}

	private static void exibirConceito(int nota, String conceito) {
		System.out.println("O conceito para a nota " + nota + " é " + conceito + ".");
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "insuficiente";
		} else if (nota <= 64) {
			conceito = "regular";
		} else if (nota <= 84) {
			conceito = "bom";
		} else {
			conceito = "ótimo";
		}
		return conceito;
	}

	// TODO Validar a nota para garantir que a mesma esteja no intervalo.
	private static int obterNota() {
		int nota;
		System.out.println("Informe uma nota de 0 a 100 (intervalo fechado):");
		nota = sc.nextInt();
		return nota;
	}

}
