package br.ucsal.bes20211.poo.lista01;

import java.util.Scanner;

public class Questao01V2 {

	/*
	 * 
	 * Suponha que o conceito de um aluno seja determinado em função da sua nota.
	 * Suponha, também, que esta nota seja um valor inteiro na faixa de 0 a 100
	 * (intervalo fechado), conforme a seguinte faixa:
	 * 
	 * Nota Conceito 0 a 49 Insuficiente 50 a 64 Regular 65 a 84 Bom 85 a 100 Ótimo
	 * 
	 * Crie um programa em Java que leia a nota de um aluno e apresente o conceito
	 * do mesmo. Não é necessário tratar valores fora da faixa.
	 * 
	 */

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(nota, conceito);
	}

	private static int obterNota() {
		int nota;
		while (true) {
			System.out.println("Informe a nota (0 a 100, intervalo fechado):");
			nota = sc.nextInt();
			if (nota >= 0 && nota <= 100) {
				return nota;
			} else {
				System.out.println("Nota fora da faixa.");
			}
		}
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "insuficiente";
		} else if (nota <= 64) {
			conceito = "regular";
		} else if (nota <= 84) {
			conceito = "bom";
		} else {
			conceito = "ótimo";
		}
		return conceito;
	}

	private static void exibirConceito(int nota, String conceito) {
		System.out.println("O conceito para a nota " + nota + " é " + conceito + ".");
	}

}
