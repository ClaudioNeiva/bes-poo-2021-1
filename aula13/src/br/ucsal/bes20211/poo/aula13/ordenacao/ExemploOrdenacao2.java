package br.ucsal.bes20211.poo.aula13.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExemploOrdenacao2 {

	public static void main(String[] args) {
		ordenacao();
	}

	private static void ordenacao() {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.add(new Pessoa("Claudio", "Neiva", 1995));
		pessoas.add(new Pessoa("Robert", "Matos", 2003));
		pessoas.add(new Pessoa("Fernando", "Damasceno", 1980));
		pessoas.add(new Pessoa("Antonio", "Neiva", 1973));
		pessoas.add(new Pessoa("Bruno", "Vieira", 1995));
		pessoas.add(new Pessoa("Lucilandia", "Babani", 2002));
		pessoas.add(new Pessoa("Davi", "Matos", 1990));
		pessoas.add(new Pessoa("Natalia", "Matos", 2000));

		System.out.println("\nPessoas na ordem que foram informados:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));

		pessoas.sort(Comparator.naturalOrder());
		System.out.println("\nPessoas em ordem crescente de NOME:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));

		pessoas.sort(Comparator.reverseOrder());
		System.out.println("\nPessoas em ordem decrescente de NOME:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));

		pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento));
		System.out.println("\nPessoas em ordem crescente de ANO DE NASCIMENTO:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));

		pessoas.sort(Comparator.comparing(Pessoa::getAnoNascimento).reversed());
		System.out.println("\nPessoas em ordem decrescente de ANO DE NASCIMENTO:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));

		pessoas.sort(Comparator.comparing(Pessoa::getSobrenome).thenComparing(Pessoa::getNome));
		System.out.println("\nPessoas em ordem crescente de sobrenome e nome:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));
		
		// Como era feito até o Java 7.
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				int resultado = o1.getSobrenome().compareTo(o2.getSobrenome());
				if(resultado==0) {
					resultado = o1.getNome().compareTo(o2.getNome());
				}
				return resultado;
			}
		});
		System.out.println("\nPessoas em ordem crescente de sobrenome e nome:");
		pessoas.forEach(pessoa -> System.out.println(pessoa));

	}
}
