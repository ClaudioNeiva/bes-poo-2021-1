package br.ucsal.bes20211.poo.aula13.ordenacao;

public class Pessoa implements Comparable<Pessoa> {

	private String nome;

	private String sobrenome;

	private Integer anoNascimento;

	public Pessoa(String nome, String sobrenome, Integer anoNascimento) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.anoNascimento = anoNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", sobrenome=" + sobrenome + ", anoNascimento=" + anoNascimento + "]";
	}

	// -1 -> e o objeto atual for MENOR que o objeto passado como parâmetro
	//  0 -> e o objeto atual for IGUAL ao objeto passado como parâmetro
	//  1 -> e o objeto atual for MAIOR que o objeto passado como parâmetro
	@Override
	public int compareTo(Pessoa o) {
		return nome.compareTo(o.nome);
	}

}
