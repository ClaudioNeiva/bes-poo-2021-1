package br.ucsal.bes20211.poo.aula13.ordenacao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ExemploOrdenacao1 {

	public static void main(String[] args) {
		ordenacaoAPartirJava8();
	}

	private static void ordenacaoAPartirJava8() {
		List<String> nomes = new ArrayList<>();
		nomes.add("Claudio");
		nomes.add("Maria");
		nomes.add("Ana");
		nomes.add("Pedro");
		nomes.add("Antonio");
		nomes.add("Joao");

		// exemploUsoStream(nomes);

		System.out.println("\nNomes na ordem que foram informados:");
		nomes.forEach(nome -> System.out.println(nome));

		nomes.sort(Comparator.naturalOrder());
		System.out.println("\nNomes em ordem crescente (baseado na tabela ASCII):");
		nomes.forEach(nome -> System.out.println(nome));
		
		nomes.sort(Comparator.reverseOrder());
		System.out.println("\nNomes em ordem decrescente (baseado na tabela ASCII):");
		nomes.forEach(nome -> System.out.println(nome));
		
	}

	private static void exemploUsoStream(List<String> nomes) {
		System.out.println("\nFiltro1:");
		nomes.stream().filter(n -> n.length() > 5 || n.contains("na")).forEach(System.out::println);

		System.out.println("\nFiltro2:");
		List<String> nomesFiltrados = nomes.stream().filter(n -> n.length() > 5 || n.contains("na")).collect(Collectors.toList());
		nomesFiltrados.forEach(System.out::println);
		
		System.out.println("\nNomes na ordem que foram informados:");
		nomes.forEach(System.out::println);

		System.out.println("\nNomes na ordem que foram informados:");
		nomes.forEach(n -> System.out.println(n));

		System.out.println("\nNomes na ordem que foram informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
