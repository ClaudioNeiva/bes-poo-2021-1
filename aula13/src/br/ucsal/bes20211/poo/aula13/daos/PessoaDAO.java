package br.ucsal.bes20211.poo.aula13.daos;

import java.util.ArrayList;
import java.util.List;

// Alternativa à criação de DAOs separados (PessoaFisicaDAO e PessoaJuridicaDAO).
public class PessoaDAO {

	private static List<Pessoa> pessoas = new ArrayList<>();

	public static void adicionar(Pessoa pessoa) {
		pessoas.add(pessoa);
	}

	public static List<Pessoa> obterTodas() {
		return new ArrayList<>(pessoas);
	}

}
