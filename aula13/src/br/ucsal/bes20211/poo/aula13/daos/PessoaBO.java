package br.ucsal.bes20211.poo.aula13.daos;

import java.util.ArrayList;
import java.util.List;

public class PessoaBO {

	public List<Pessoa> obterTodas() {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.addAll(PessoaFisicaDAO.obterTodas());
		pessoas.addAll(PessoaJuridicaDAO.obterTodas());
		return pessoas;
	}

}
