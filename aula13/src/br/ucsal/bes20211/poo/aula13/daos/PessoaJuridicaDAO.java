package br.ucsal.bes20211.poo.aula13.daos;

import java.util.ArrayList;
import java.util.List;

public class PessoaJuridicaDAO {

	private static List<PessoaJuridica> pessoasJuridicas = new ArrayList<>();

	public static void adicionar(PessoaJuridica pessoaJuridica) {
		pessoasJuridicas.add(pessoaJuridica);
	}

	public static List<PessoaJuridica> obterTodas() {
		return new ArrayList<>(pessoasJuridicas);
	}

}
