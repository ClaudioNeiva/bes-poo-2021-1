package br.ucsal.bes20211.poo.aula13.daos;

import java.util.ArrayList;
import java.util.List;

public class PessoaFisicaDAO {

	private static List<PessoaFisica> pessoasFisicas = new ArrayList<>();

	public static void adicionar(PessoaFisica pessoaFisica) {
		pessoasFisicas.add(pessoaFisica);
	}

	public static List<PessoaFisica> obterTodas() {
		return new ArrayList<>(pessoasFisicas);
	}

}
