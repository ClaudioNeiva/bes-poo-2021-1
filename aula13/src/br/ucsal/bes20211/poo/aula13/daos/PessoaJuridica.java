package br.ucsal.bes20211.poo.aula13.daos;

import java.util.List;

public class PessoaJuridica extends Pessoa {

	private String cnpj;

	private Integer inscricaoEstadual;

	private Integer inscricaoMunicipal;

	private List<String> cnaes;

	public PessoaJuridica(String nome, String endereco, List<String> telefones, String email, String cnpj,
			Integer inscricaoEstadual, Integer inscricaoMunicipal, List<String> cnaes) {
		super(nome, endereco, telefones, email);
		this.cnpj = cnpj;
		this.inscricaoEstadual = inscricaoEstadual;
		this.inscricaoMunicipal = inscricaoMunicipal;
		this.cnaes = cnaes;
	}

	public String obterNI() {
		return cnpj;
	}	
	
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Integer getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(Integer inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Integer getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(Integer inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public List<String> getCnaes() {
		return cnaes;
	}

	public void setCnaes(List<String> cnaes) {
		this.cnaes = cnaes;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", inscricaoEstadual=" + inscricaoEstadual + ", inscricaoMunicipal="
				+ inscricaoMunicipal + ", cnaes=" + cnaes + ", toString()=" + super.toString() + "]";
	}

}
