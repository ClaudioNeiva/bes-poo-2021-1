package br.ucsal.bes20211.poo.aula13.listas;

import java.util.List;

public abstract class Pessoa {

	private String nome;

	private String endereco;

	private List<String> telefones;

	private String email;

	public Pessoa(String nome, String endereco, List<String> telefones, String email) {
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.telefones = telefones;
		this.email = email;
	}

	/**
	 * NI é o número identificador, utilizado geralmente na comunicação RFB.
	 * 
	 * @return CPF ou CNPJ ou outro identificador aceito pela RFB.
	 */
	public abstract String obterNI();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", endereco=" + endereco + ", telefones=" + telefones + ", email=" + email
				+ "]";
	}

}
