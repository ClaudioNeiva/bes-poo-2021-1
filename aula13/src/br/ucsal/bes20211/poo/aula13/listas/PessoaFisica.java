package br.ucsal.bes20211.poo.aula13.listas;

import java.time.LocalDate;
import java.util.List;

public class PessoaFisica extends Pessoa {

	private String cpf;

	private LocalDate dataNascimento;

	private String nomeMae;

	public PessoaFisica(String nome, String endereco, List<String> telefones, String email, String cpf,
			LocalDate dataNascimento, String nomeMae) {
		super(nome, endereco, telefones, email);
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
		this.nomeMae = nomeMae;
	}
	
	public String obterNI() {
		return cpf;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	@Override
	public String toString() {
		return "PessoaFisica [cpf=" + cpf + ", dataNascimento=" + dataNascimento + ", nomeMae=" + nomeMae
				+ ", toString()=" + super.toString() + "]";
	}

}
