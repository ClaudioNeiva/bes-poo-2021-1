package br.ucsal.bes20211.poo.aula13.listas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExemploLista {

	public static void main(String[] args) {

		List<Pessoa> pessoas = new ArrayList<>();

		PessoaFisica pessoaFisica = new PessoaFisica("Claudio", "Rua x", null, "claudio@ucsal.br", "123123",
				LocalDate.of(2001, 8, 5), "Lais");
		PessoaJuridica pessoaJuridica = new PessoaJuridica("Loja X", "Rua y", Arrays.asList("123123", "213124"),
				"loja@umacoisa.com.br", "098798798", 123, 456, Arrays.asList("1234123", "56756756", "56236798"));

		pessoas.add(pessoaFisica);
		pessoas.add(pessoaJuridica);

		for (Pessoa pessoa : pessoas) {
			if (pessoa instanceof PessoaFisica) {
				PessoaFisica pf = (PessoaFisica) pessoa;
				System.out.println(
						"A pessoa " + pessoa.getNome() + " é uma pessoa física. Nome da mãe=" + pf.getNomeMae());
			} else if (pessoa instanceof PessoaJuridica) {
				PessoaJuridica pj = (PessoaJuridica) pessoa;
				System.out.println("A pessoa " + pessoa.getNome() + " é uma pessoa juridica. Inscrição estadual="
						+ pj.getInscricaoEstadual());
			}
		}

	}

}
