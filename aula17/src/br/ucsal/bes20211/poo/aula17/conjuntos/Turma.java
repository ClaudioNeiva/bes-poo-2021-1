package br.ucsal.bes20211.poo.aula17.conjuntos;

import java.util.HashSet;
import java.util.Set;

public class Turma {

	private Integer codigo;

	// HashSet, TreeSet, LinkedHashSet
	private Set<Aluno> alunos = new HashSet<>();

	public Turma(Integer codigo) {
		super();
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Set<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(Set<Aluno> alunos) {
		this.alunos = alunos;
	}

	@Override
	public String toString() {
		return "Turma [codigo=" + codigo + ", alunos=" + alunos + "]";
	}

}
