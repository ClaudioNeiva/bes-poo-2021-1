package br.ucsal.bes20211.poo.aula17.conjuntos;

public class ExemploConjuntosTurmaAluno {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno(1, "Claudio");
		Aluno aluno2 = new Aluno(2, "Antonio");
		Aluno aluno3 = new Aluno(3, "Pedreira");
		Aluno aluno4 = new Aluno(4, "Neiva");

		Turma turma1 = new Turma(100);
		System.out.println("aluno1.hashcode=" + aluno1.hashCode());
		turma1.getAlunos().add(aluno1);
		System.out.println("aluno2.hashcode=" + aluno2.hashCode());
		turma1.getAlunos().add(aluno2);
		turma1.getAlunos().add(aluno3);
		turma1.getAlunos().add(aluno4);

		System.out.println("\nPesquisando se existe...");
		System.out.println("aluno1.hashcode=" + aluno1.hashCode());
		System.out.println("turma1 contém aluno1 = " + turma1.getAlunos().contains(aluno1));

		aluno1.setNome("Maria");
		System.out.println("\nPesquisando se existe...");
		System.out.println("aluno1.hashcode=" + aluno1.hashCode());
		System.out.println("turma1 contém aluno1 = " + turma1.getAlunos().contains(aluno1));
	}

}
