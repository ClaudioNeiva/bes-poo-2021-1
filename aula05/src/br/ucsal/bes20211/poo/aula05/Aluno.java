package br.ucsal.bes20211.poo.aula05;

public class Aluno {

	private String nome;

	private Integer anoNascimento;

	private String email;

	private String endereco;

	private Integer matricula;

	public Aluno(String nome, Integer anoNascimento, String email, String endereco, Integer matricula) {
		this.nome = nome;
		this.anoNascimento = anoNascimento;
		this.email = email;
		this.endereco = endereco;
		this.matricula = matricula;
	}

	public Aluno(String nome) {
		this.nome = nome;
		System.out.println("Você pode fazer alguma coisa aqui!");
	}

	public Aluno(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public Aluno(String endereco, String email) {
		this.endereco = email;
		this.email = email;
	}

	public Aluno(Integer matricula, String nome) {
		this(nome);
		this.matricula = matricula;
	}

	public String apresentar() {
		String descricaoEndereco = descreverEndereco();
		String descricaoAnoNascimento = descreverAnoNacimento();
		return "Nome: " + nome + "; endereco: " + descricaoEndereco + "; anoNascimento: " + descricaoAnoNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	private String descreverEndereco() {
		String descricaoEndereco;
		if (endereco == null) {
			descricaoEndereco = "(sem endereço cadastrado)";
		} else {
			descricaoEndereco = endereco;
		}
		return descricaoEndereco;
	}

	private String descreverAnoNacimento() {
		String descricaoAnoNascimento;
		if (anoNascimento == null) {
			descricaoAnoNascimento = "(sem ano de nascimento cadastrado)";
		} else {
			descricaoAnoNascimento = anoNascimento.toString();
		}
		return descricaoAnoNascimento;
	}

}
