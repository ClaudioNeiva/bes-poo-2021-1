package br.ucsal.bes20211.poo.aula05;

public class Exemplo {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno(123, "Maria da Silva");

		aluno1.setEndereco("Rua X");

		System.out.println("aluno1=" + aluno1.apresentar());

		System.out.println("aluno1.nome=" + aluno1.getNome());

	}

}
