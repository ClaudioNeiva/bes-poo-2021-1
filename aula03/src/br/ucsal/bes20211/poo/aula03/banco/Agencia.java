package br.ucsal.bes20211.poo.aula03.banco;

public class Agencia {

	static final int QTD_MAX_CONTAS = 100;

	Integer numero;

	ContaCorrente[] contasCorrente = new ContaCorrente[QTD_MAX_CONTAS];

	int pos = 0;

	void criarContaCorrente(int codigo, String nomeCorrentista) {
		// TODO Criar um construtor parametrizado para ContaCorrente
		ContaCorrente contaCorrente = new ContaCorrente();
		contaCorrente.codigo = codigo;
		contaCorrente.nomeCorrentista = nomeCorrentista;

		contasCorrente[pos] = contaCorrente;
		pos++;
	}

	void transferirValores(ContaCorrente contaCorrenteOrigem, ContaCorrente contaCorrenteDestino, double valor) {
		contaCorrenteOrigem.sacar(valor);
		contaCorrenteDestino.depositar(valor);
	}

	void listarContaCorrente() {

		for (int i = 0; i < pos; i++) {
			ContaCorrente contaCorrente = contasCorrente[i];

			// FIXME Não devemos ter comandos de apresentação de dados em classes de
			// domínio.

			System.out.println("Código=" + contaCorrente.codigo);
			System.out.println("Nome=" + contaCorrente.nomeCorrentista);
			System.out.println("Saldo=" + contaCorrente.saldo);
		}

	}
}
