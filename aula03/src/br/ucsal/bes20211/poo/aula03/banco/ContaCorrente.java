package br.ucsal.bes20211.poo.aula03.banco;

public class ContaCorrente {

	int codigo;
	
	String nomeCorrentista;
	
	double saldo;
	
	void sacar(double valor) {
		saldo -= valor;
	}
	
	void depositar(double valor) {
		saldo += valor;
	}
	
	double obterSaldo() {
		return saldo;
	}
	
}
