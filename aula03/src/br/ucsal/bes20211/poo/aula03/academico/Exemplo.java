package br.ucsal.bes20211.poo.aula03.academico;

public class Exemplo {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		Aluno aluno3 = aluno1;
		
		aluno1.nome = "Claudio Neiva";
		aluno1.anoNascimento = 2000;
		aluno1.email = "antonio@ucsal.br";
		aluno1.endereco = "Rua X";
		aluno1.matricula = 123;

		aluno2.nome = "Claudio Neiva";
		aluno2.anoNascimento = 2000;
		aluno2.email = "antonio@ucsal.br";
		aluno2.endereco = "Rua X";
		aluno2.matricula = 123;

		System.out.println("aluno1=" + aluno1);
		System.out.println("aluno2=" + aluno2);
		System.out.println("aluno3=" + aluno3);

		if (aluno1 == aluno2) {
			System.out.println("O aluno1 aponta para a mesma instância apontada pelo aluno2.");
		} else {
			System.out.println("O aluno1 aponta para uma instância diferente da apontada aluno2.");
		}

		if (aluno1.equals(aluno2)) {
			System.out.println("O aluno1 é igual ao aluno2.");
		} else {
			System.out.println("O aluno1 é diferente do aluno2.");
		}
		
	}

}
