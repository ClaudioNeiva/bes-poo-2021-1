package br.ucsal.bes20211.poo.aula11.tui;

import br.ucsal.bes20211.poo.aula11.domain.ContaCorrente;
import br.ucsal.bes20211.poo.aula11.domain.ContaCorrenteEspecial;
import br.ucsal.bes20211.poo.aula11.domain.ContaCorrenteInfinita;
import br.ucsal.bes20211.poo.aula11.exception.NegocioException;

public class ExemploTUI {

	public static void main(String[] args) {

		testarContaCorrente();
		System.out.println("coisas que podem ocorrer se tudo der certo!");

	}

	private static void testarContaCorrente() {

		System.out.println("########### Conta Corrente ###########");
		ContaCorrente contaCorrente1 = new ContaCorrente("Claudio Neiva");
		System.out.println("Número: " + contaCorrente1.getNumero());
		System.out.println("Correntista: " + contaCorrente1.getNomeCorrentista());
		contaCorrente1.depositar(45000d);
		sacar(contaCorrente1, 40d);
		sacar(contaCorrente1, 120d);
		System.out.println("Saldo: " + contaCorrente1.consultarSaldo());

		System.out.println("\n\n########### Conta Corrente Especial ###########");
		ContaCorrenteEspecial contaCorrenteEspecial1 = new ContaCorrenteEspecial("Deputado 1", 100000d);
		System.out.println("Número: " + contaCorrenteEspecial1.getNumero());
		System.out.println("Correntista: " + contaCorrenteEspecial1.getNomeCorrentista());
		System.out.println("Limite de crédito: " + contaCorrenteEspecial1.getLimiteCredito());
		contaCorrenteEspecial1.depositar(12000d);
		sacar(contaCorrenteEspecial1, 30d);
		sacar(contaCorrenteEspecial1, 80d);
		sacar(contaCorrenteEspecial1, 23000d);
		sacar(contaCorrenteEspecial1, 3330d);
		System.out.println("Saldo: " + contaCorrenteEspecial1.consultarSaldo());

		System.out.println("\n\n########### Conta Corrente do Bill Gates ###########");
		ContaCorrenteInfinita contaCorrenteDoTioBill = new ContaCorrenteInfinita("Bill Gates");
		contaCorrenteDoTioBill.depositar(1d);
		sacar(contaCorrenteDoTioBill, 1000000d);
		System.out.println("Saldo: " + contaCorrenteDoTioBill.consultarSaldo());
	}

	private static void sacar(ContaCorrente contaCorrente, Double valor) {
		try {
			// late binding
			contaCorrente.sacar(valor);
			if (contaCorrente instanceof ContaCorrenteEspecial) {
				System.out.println("******** Ocorreu um downcast!");
				exibirDetalhesSaldo((ContaCorrenteEspecial) contaCorrente);
			}
		} catch (NegocioException e) {
			System.out.println("Problema ao realizar o saque: " + e.getMessage());
		}
	}

	private static void exibirDetalhesSaldo(ContaCorrenteEspecial conta1) {
		System.out.println("Saldo=" + conta1.consultarSaldo());
		System.out.println("Limite de crédito=" + conta1.getLimiteCredito());
	}

}
