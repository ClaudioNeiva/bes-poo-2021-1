package br.ucsal.bes20211.poo.aula11.domain;

import br.ucsal.bes20211.poo.aula11.exception.NegocioException;

public class ContaCorrente {

	private static Integer cont = 0;

	private Integer numero;

	private String nomeCorrentista;

	protected Double saldo;

	public ContaCorrente(String nomeCorrentista) {
		super();
		this.nomeCorrentista = nomeCorrentista;
		definirNumero();
		this.saldo = 0d;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public Double sacar(Double valor) throws NegocioException {
		if (valor > saldo) {
			throw new NegocioException("Saldo insuficiente para saque de " + valor + ".");
		}
		saldo -= valor;
		return saldo;
	}

	public Double consultarSaldo() {
		return saldo;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
	}

	public Integer getNumero() {
		return numero;
	}

	private void definirNumero() {
		ContaCorrente.cont++;
		numero = cont;
	}

}
