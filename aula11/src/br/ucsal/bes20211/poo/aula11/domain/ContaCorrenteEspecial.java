package br.ucsal.bes20211.poo.aula11.domain;

import br.ucsal.bes20211.poo.aula11.exception.NegocioException;

public class ContaCorrenteEspecial extends ContaCorrente {

	private Double limiteCredito;

	public ContaCorrenteEspecial(String nomeCorrentista, Double limiteCredito) {
		super(nomeCorrentista);
		this.limiteCredito = limiteCredito;
	}
	
	@Override
	public Double sacar(Double valor) throws NegocioException {
		if (valor > saldo + limiteCredito) {
			throw new NegocioException("Saldo, incluindo o limite de crédito, é insuficiente para saque de " + valor + ".");
		}
		saldo -= valor;
		return saldo;
	}

	public Double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

}
