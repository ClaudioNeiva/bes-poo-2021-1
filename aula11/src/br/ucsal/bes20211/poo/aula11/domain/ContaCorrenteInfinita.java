package br.ucsal.bes20211.poo.aula11.domain;

import br.ucsal.bes20211.poo.aula11.exception.NegocioException;

public class ContaCorrenteInfinita extends ContaCorrente {

	public ContaCorrenteInfinita(String nomeCorrentista) {
		super(nomeCorrentista);
	}
 
	@Override
	public Double sacar(Double valor) throws NegocioException {
		saldo -= valor;
		return saldo;
	}

}
