package br.ucsal.bes20211.poo.aula12;

import java.time.LocalDate;

public class Exemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica = new PessoaFisica("Claudio", "Rua X", null, "claudio@ucsal.br", "123",
				LocalDate.of(1, 2, 2000), "maria");

		System.out.println(pessoaFisica);

		emitirNotaFiscal(pessoaFisica);
		
		Medico medico = new Medico("Ana", "Rua y", null, "ana@hospital.br", "123123", LocalDate.of(1, 2, 1990), "Clara", "35554-BA");
		
		validarConselhoClasse(medico);
		
	}

	public static void emitirNotaFiscal(Pessoa pessoa) {
		System.out.println("Número Indentificador [NI] (CPF/CNPJ):" + pessoa.obterNI());
		System.out.println("nome:" + pessoa.getNome());
		System.out.println("endereço:" + pessoa.getEndereco());
	}

	public static void validarConselhoClasse(ProfissionalConselhoClasse profissional) {
		// Receber uma instância de uma classe que possua inscrição em um
		// conselho de classe e vai enviar uma mensagem para a api de
		// conselho.
		System.out.println("Número de inscrição no conselho: " + profissional.obterInscricaoConselho());
	}

}
