package br.ucsal.bes20211.poo.aula12;

import java.util.List;

public class ET extends Pessoa {

	private String niET;
	
	public ET(String nome, String endereco, List<String> telefones, String email) {
		super(nome, endereco, telefones, email);
	}

	@Override
	public String obterNI() {
		return niET;
	}

}
