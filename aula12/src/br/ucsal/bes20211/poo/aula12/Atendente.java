package br.ucsal.bes20211.poo.aula12;

import java.time.LocalDate;
import java.util.List;

public class Atendente extends PessoaFisica {

	private Integer qtdHoras;

	public Atendente(String nome, String endereco, List<String> telefones, String email, String cpf,
			LocalDate dataNascimento, String nomeMae, Integer qtdHoras) {
		super(nome, endereco, telefones, email, cpf, dataNascimento, nomeMae);
		this.qtdHoras = qtdHoras;
	}

	public Integer getQtdHoras() {
		return qtdHoras;
	}

	public void setQtdHoras(Integer qtdHoras) {
		this.qtdHoras = qtdHoras;
	}

}
