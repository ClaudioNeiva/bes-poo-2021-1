package br.ucsal.bes20211.poo.aula12;

import java.time.LocalDate;
import java.util.List;

public class Paciente extends PessoaFisica {

	private String numeroConvenio;

	public Paciente(String nome, String endereco, List<String> telefones, String email, String cpf,
			LocalDate dataNascimento, String nomeMae, String numeroConvenio) {
		super(nome, endereco, telefones, email, cpf, dataNascimento, nomeMae);
		this.numeroConvenio = numeroConvenio;
	}

	public String getNumeroConvenio() {
		return numeroConvenio;
	}

	public void setNumeroConvenio(String numeroConvenio) {
		this.numeroConvenio = numeroConvenio;
	}

}
