package br.ucsal.bes20211.poo.aula12;

public interface ProfissionalConselhoClasse {

	String obterInscricaoConselho();

	default Integer calcularAlgumaCoisa(Integer a, Integer b) {
		return a + 2 * b;
	}

}
