package br.ucsal.bes20211.poo.aula12;

import java.time.LocalDate;
import java.util.List;

public class Medico extends PessoaFisica implements ProfissionalConselhoClasse, Contribuinte {

	private String crm;

	public Medico(String nome, String endereco, List<String> telefones, String email, String cpf,
			LocalDate dataNascimento, String nomeMae, String crm) {
		super(nome, endereco, telefones, email, cpf, dataNascimento, nomeMae);
		this.crm = crm;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	@Override
	public String obterInscricaoConselho() {
		return crm;
	}

	@Override
	public Integer calcularAlgumaCoisa(Integer a, Integer b) {
		return 100 * a + 1000 * b;
	}

}
