package br.ucsal.bes20211.poo.aula12;

import java.time.LocalDate;
import java.util.List;

public class Enfermeiro extends PessoaFisica implements ProfissionalConselhoClasse {

	private String coren;

	public Enfermeiro(String nome, String endereco, List<String> telefones, String email, String cpf,
			LocalDate dataNascimento, String nomeMae, String coren) {
		super(nome, endereco, telefones, email, cpf, dataNascimento, nomeMae);
		this.coren = coren;
	}

	public String getCoren() {
		return coren;
	}

	public void setCoren(String coren) {
		this.coren = coren;
	}

	@Override
	public String obterInscricaoConselho() {
		return coren;
	}

}
