package br.ucsal.bes20211.poo.aula08.enumeracoes;

import java.util.Scanner;

public class MenuTUI {

	private static final Scanner SCANNER = new Scanner(System.in);

	public static void executar() {
		MenuOpcoesEnum opcaoSelecionada;
		do {
			exibir();
			opcaoSelecionada = obterOpcaoDeseja();
			executarOpcaoDesejada(opcaoSelecionada);

		} while (!opcaoSelecionada.equals(MenuOpcoesEnum.SAIR));
	}

	private static void exibir() {
		System.out.println("################# AGENDA #################");
		for (MenuOpcoesEnum menuOpcao : MenuOpcoesEnum.values()) {
			System.out.println(menuOpcao.obterDescricaoCompleta());
		}
	}

	private static MenuOpcoesEnum obterOpcaoDeseja() {
		System.out.println("Informe o código da ação desejada:");
		Integer opcaoInteger = SCANNER.nextInt();
		return MenuOpcoesEnum.valueOfCodigo(opcaoInteger);
	}

	private static void executarOpcaoDesejada(MenuOpcoesEnum opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case CADASTRAR:
			AgendaTUI.cadastrar();
			break;
		case PESQUISAR:
			AgendaTUI.pesquisar();
			break;
		case EXCLUIR:
			AgendaTUI.excluir();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		default:
			System.out.println("Não implementada!");
			break;
		}
	}

}
