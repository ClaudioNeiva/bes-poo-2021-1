package br.ucsal.bes20211.poo.aula08.enumeracoes;

import java.util.Scanner;

// ATENÇÃO: NÃO FAÇA ASSIM!!!!

public class MenuNaoFacaAssimTUI {

	private static final Scanner SCANNER = new Scanner(System.in);

	public static void executar() {
		Integer opcaoSelecionada;
		do {
			System.out.println("################# AGENDA #################");
			System.out.println("1 - Cadastrar");
			System.out.println("2 - Pesquisar");
			System.out.println("3 - Excluir");
			System.out.println("9 - Sair");
			System.out.println("Informe a opções desejada:");
			opcaoSelecionada = SCANNER.nextInt();
			switch (opcaoSelecionada) {
			case 1:
				System.out.println("###### CADASTRO ###### ");
				// Aqui todos os códigos para o cadastro!
				break;
			case 2:
				System.out.println("###### PESQUISA ###### ");
				// Aqui todos os códigos para o pesquisa!
				break;
			case 3:
				System.out.println("###### EXCLUSÃO ###### ");
				// Aqui todos os códigos para o exclusão!
				break;
			case 9:
				System.out.println("Bye...");
			default:
				System.out.println("Opção inválida!");
				break;
			}
		} while(opcaoSelecionada != 9);
	}

}
