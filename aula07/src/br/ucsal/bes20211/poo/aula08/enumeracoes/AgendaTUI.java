package br.ucsal.bes20211.poo.aula08.enumeracoes;

public class AgendaTUI {

	public static void cadastrar() {
		System.out.println("###### CADASTRO ###### ");
	}

	public static void pesquisar() {
		System.out.println("###### PESQUISA ###### ");
	}

	public static void excluir() {
		System.out.println("###### EXCLUSÃO ###### ");
	}

}
