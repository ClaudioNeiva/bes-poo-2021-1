package br.ucsal.bes20211.poo.aula08.enumeracoes;

public enum MenuOpcoesEnum {

	CADASTRAR(1, "Cadastrar"),

	EXCLUIR(2, "Excluir"),

	PESQUISAR(3, "Pesquisar"),

	LIMPAR(4, "Limpar"),

	SAIR(9, "Sair");

	private Integer codigo;

	private String descricao;

	private MenuOpcoesEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public String obterDescricaoCompleta() {
		return codigo + " - " + descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static MenuOpcoesEnum valueOfCodigo(Integer codigoSelecionado) {
		for (MenuOpcoesEnum menuOpcao : values()) {
			if (menuOpcao.codigo.equals(codigoSelecionado)) {
				return menuOpcao;
			}
		}
		// FIXME Lançar uma exception!
		return null;
	}

}
