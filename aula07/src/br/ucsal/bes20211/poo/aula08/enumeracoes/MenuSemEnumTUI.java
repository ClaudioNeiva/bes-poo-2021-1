package br.ucsal.bes20211.poo.aula08.enumeracoes;

import java.util.Scanner;

public class MenuSemEnumTUI {

	private static final Scanner SCANNER = new Scanner(System.in);

	public static void executar() {
		Integer opcaoSelecionada;
		do {
			exibir();
			opcaoSelecionada = obterOpcaoDeseja();
			executarOpcaoDesejada(opcaoSelecionada);
		} while(opcaoSelecionada != 9);
	}

	private static void exibir() {
		System.out.println("################# AGENDA #################");
		System.out.println("1 - Cadastrar");
		System.out.println("2 - Pesquisar");
		System.out.println("3 - Excluir");
		System.out.println("9 - Sair");
	}

	private static Integer obterOpcaoDeseja() {
		System.out.println("Informe a opções desejada:");
		return SCANNER.nextInt();
	}

	private static void executarOpcaoDesejada(Integer opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case 1:
			AgendaTUI.cadastrar();
			break;
		case 2:
			AgendaTUI.pesquisar();
			break;
		case 3:
			AgendaTUI.excluir();
			break;
		case 9:
			System.out.println("Bye...");
		default:
			System.out.println("Opção inválida!");
			break;
		}
	}

}
