package br.ucsal.bes20211.poo.aula08.listas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ExemploListaNomes {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		nomes.add("claudio");
		nomes.add("pedreira");
		nomes.add("antonio");
		nomes.add("neiva");
		nomes.add("maria");

		System.out.println("Nomes original:" + nomes);

		nomes.sort(Comparator.naturalOrder());

		System.out.println("Nomes ordenado:" + nomes);
	}

}
