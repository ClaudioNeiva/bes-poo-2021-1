package br.ucsal.bes20211.poo.aula08.listas;

import java.util.List;

public class AlunoExemplo {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno("Claudio", 1973, "claudio.neiva@pro.ucsal.br", "Rua X");

		AlunoDAO.insert(aluno1);

		List<Aluno> todosAlunos = AlunoDAO.findAll();
		todosAlunos.add(new Aluno("Maria", 2000, "maria@ucsal", "Rua y"));
		todosAlunos.get(0).setNome("Joaquim");

		System.out.println("AlunosDAO.size()=" + AlunoDAO.findByPosition(0).getNome());
		System.out.println("AlunosDAO.size()=" + AlunoDAO.size());
		System.out.println("todosAlunos.size()=" + todosAlunos.size());

	}

}
