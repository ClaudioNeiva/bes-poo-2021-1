package br.ucsal.bes20211.poo.aula08.listas;

import java.util.ArrayList;
import java.util.List;

public class AlunoDAO {

	private static List<Aluno> alunos = new ArrayList<>();

	private AlunoDAO() {
	}

	public static void insert(Aluno aluno) {
		alunos.add(aluno);
	}

	public static void delete(Aluno aluno) {
		alunos.remove(aluno);
	}

	public static Aluno findByMatricula(Integer matricula) {
		for (Aluno aluno : alunos) {
			if (aluno.getMatricula().equals(matricula)) {
				return aluno;
			}
		}
		// FIXME Lançar uma exceção.
		return null;
	}

	public static Integer size() {
		return alunos.size();
	}

	public static Aluno findByPosition(int pos) {
		return alunos.get(pos);
	}
	
	public static void clear() {
		alunos.clear();
	}
	
	public static Aluno findByNome(String nome) {
		return null;
	}

	public static List<Aluno> findAll() {
		return new ArrayList<>(alunos);
	}

}
