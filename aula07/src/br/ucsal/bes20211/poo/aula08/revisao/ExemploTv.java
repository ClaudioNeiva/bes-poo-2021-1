package br.ucsal.bes20211.poo.aula08.revisao;

import java.time.LocalDate;

public class ExemploTv {

	public static void main(String[] args) {
		Tv tv = new Tv();
//		tv.

		// Tipo de dado composto heterogêneo => estruturas (struct, record)
		// Tipo de dado composto homogênio => vetores (arranjos) (array)
		Aluno aluno = new Aluno();
		aluno.matricula = 123;
		aluno.dataNascimento = LocalDate.of(2000, 5, 3);
		aluno.nome = "Claudio";
	}

}
