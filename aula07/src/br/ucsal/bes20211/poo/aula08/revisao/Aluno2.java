package br.ucsal.bes20211.poo.aula08.revisao;

import java.time.LocalDate;

public class Aluno2 {

	private Integer matricula;

	private String nome;

	private LocalDate dataNascimento;

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
