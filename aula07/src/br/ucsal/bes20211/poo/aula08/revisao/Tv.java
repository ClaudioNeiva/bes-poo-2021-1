package br.ucsal.bes20211.poo.aula08.revisao;

public class Tv {

	private static final int VOLUME_MIN = 0;
	private static final int VOLUME_MAX = 100;
	private static final Integer CANAL_MAX = 800;
	private static final Integer CANAL_MIN = 0;

	private Integer canal;

	private Integer volume;

	public Integer obterCanalAtual() {
		return canal;
	}

	public void subirCanal() {
		if (canal < CANAL_MAX) {
			this.canal++;
			// GPIOService.definirPorta1(this.canal);
		}
	}

	public void descerCanal() {
		if (canal > CANAL_MIN) {
			this.canal--;
		}
	}

	public void mudarCanal(Integer canal) {
		if (canal <= CANAL_MAX && canal >= CANAL_MIN) {
			this.canal = canal;
		}
	}

	public Integer obterVolumeAtual() {
		return volume;
	}

	public void aumentarVolume() {
		if (volume < VOLUME_MAX) {
			volume++;
		}
	}

	public void diminuirVolume() {
		if (volume > VOLUME_MIN) {
			volume--;
		}
	}

	public void zerarVolume() {
		volume = 0;
	}

}
