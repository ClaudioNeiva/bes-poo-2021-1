package br.ucsal.bes20211.poo.aula06;

public class Exemplo {

	public void metodoInstancia() {
		System.out.println("método de instância");
	}
	
	public static void metodoEstatico() {
		System.out.println("método estático");
	}
	
	public static void main(String[] args) {

		Exemplo exemplo1 = new Exemplo();
		exemplo1.metodoInstancia();
		
		Exemplo.metodoEstatico();
		metodoEstatico();
		
		System.out.println("Chamar metodoEstatico:" + Aluno.metodoEstatico());

		System.out.println("O valor inicial de Aluno.seq:" + Aluno.getSeq());

		Aluno alunoClaudio = new Aluno("Claudio", 2000, "claudio@ucsal.br", "Rua X");
		Aluno alunoBruno = new Aluno("Bruno", 2010, "bruno@ucsal.br", "Rua W");
		Aluno alunoRaul = new Aluno("Raul", 2012, "raul@ucsal.br", "Rua K");
		Aluno alunoDavi = new Aluno("Davia", 2020, "davi@ucsal.br", "Rua L");

		System.out.println("alunoClaudio=" + alunoClaudio);
		System.out.println("alunoBruno=" + alunoBruno);
		System.out.println("alunoRaul=" + alunoRaul);
		System.out.println("alunoDavi=" + alunoDavi);

		AlunoDAO.insert(alunoClaudio);
		AlunoDAO.insert(alunoBruno);
		
		
		System.out.println("A última Aluno.seq gerada:" + Aluno.getSeq());

		System.out.println("Chamar metodoEstatico:" + Aluno.metodoEstatico());
		
	}

}
