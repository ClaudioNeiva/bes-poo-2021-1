package br.ucsal.bes20211.poo.aula06;

import java.util.ArrayList;
import java.util.List;

public class AlunoDAO {

	private static List<Aluno> alunos = new ArrayList<>();

	private AlunoDAO() {
	}

	public static void insert(Aluno aluno) {
		alunos.add(aluno);
	}

	public static void delete(Aluno aluno) {
		alunos.remove(aluno);
	}

	public static Aluno findByMatricula(Integer matricula) {
		return null;
	}

	public static Aluno findByNome(String nome) {
		return null;
	}

	public static Aluno[] findAll() {
		return null;
	}

}
