package br.ucsal.bes20211.poo.calculos;

public class CalculoUtil {

	/**
	 * 
	 * Definição fatorial(n):
	 * 
	 * n pertercer ao conjunto dos número Naturais (N)
	 * 
	 * se n = 0 => fatorial = 1
	 * 
	 * se n > 0 => fatorial = n * fatorial( n - 1 )
	 * 
	 * 0! = 1
	 * 
	 * 1! = 1
	 * 
	 * 5! = 5 * 4 * 3 * 2 * 1 = 120
	 * 
	 * @param n número sobre o qual será calculado o fatorial
	 * @return o valor do fatorial do número n
	 */
	public static Long calcularFatorial(int n) {
		if (n == 0) {
			return 1L;
		}
		return n * calcularFatorial(n - 1);
	}

}
