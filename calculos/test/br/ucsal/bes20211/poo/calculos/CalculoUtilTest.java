package br.ucsal.bes20211.poo.calculos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtilTest {

	@Test
	public void testarFatorial0() {

		// Dados de entrada (caso de teste)
		int n = 0;

		// Saída esperada (caso de teste)
		Long fatorialEsperado = 1L;

		// Executando o método que eu quero testar e obtendo a saída atual
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial1() {

		int n = 1;

		Long fatorialEsperado = 1L;

		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial3() {

		int n = 3;

		Long fatorialEsperado = 6L;

		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial5() {

		int n = 5;

		Long fatorialEsperado = 120L;

		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
