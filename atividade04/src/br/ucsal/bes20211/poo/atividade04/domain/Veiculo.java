package br.ucsal.bes20211.poo.atividade04.domain;

public class Veiculo {

	// FIXME Vamos definir a visibilidade dos atributos como "public" apenas
	// temporariamente. Na próxima aula, falaremos sobre encampsulamento, e
	// modificaremos a visibilidade para private.

	public String placa;

	public Integer anoFabricacao;

	public Double valor;

	public Pessoa proprietario;

}
