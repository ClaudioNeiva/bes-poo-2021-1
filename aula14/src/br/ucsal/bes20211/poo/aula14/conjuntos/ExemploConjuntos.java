package br.ucsal.bes20211.poo.aula14.conjuntos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ExemploConjuntos {

	private static final int QTD_NUMEROS = 5;

	private static final Scanner SCANNER = new Scanner(System.in);

	/*
	 * Crie um programa em Java que solicite do usuário 5 números distintos.
	 * Enquanto não forem informados 5 números distintos, continue solicitando
	 * números ao usuário. Ao final, você deve listar os 5 números distintos
	 * informados.
	 */

	// 1, 6, 8, 8, 1, 1, 1, 8, 8, 10, 1, 8, 6, 7

	public static void main(String[] args) {
		// obterExibirNumerosDistintosUsandoList(QTD_NUMEROS);
		obterExibirNumerosDistintosUsandoSet(QTD_NUMEROS);
	}

	private static void obterExibirNumerosDistintosUsandoSet(int qtd) {
		Set<Integer> numerosDistintos = new HashSet<>(); 
		System.out.println("Informe " + qtd + " números distintos:");
		do {
			numerosDistintos.add(SCANNER.nextInt());
		} while (numerosDistintos.size() != qtd);
		System.out.println("Os números distintos informados foram: " + numerosDistintos);
	}

	private static void obterExibirNumerosDistintosUsandoList(int qtd) {
		List<Integer> numerosDistintos = new ArrayList<>();
		System.out.println("Informe " + qtd + " números distintos:");
		do {
			Integer n = SCANNER.nextInt();
			if (!numerosDistintos.contains(n)) {
				numerosDistintos.add(n);
			}
		} while (numerosDistintos.size() != qtd);
		System.out.println("Os números distintos informados foram: " + numerosDistintos);
	}
}
