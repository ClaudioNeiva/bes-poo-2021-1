package br.ucsal.bes20211.poo.aula14.conjuntos;

public class ExemploConjuntosTurmaAluno {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno(1, "Claudio");
		Aluno aluno2 = new Aluno(2, "Antonio");
		Aluno aluno3 = new Aluno(3, "Pedreira");
		Aluno aluno4 = new Aluno(1, "Claudio");
		Aluno aluno5 = new Aluno(4, "Neiva");

		Turma turma1 = new Turma(100);
		turma1.getAlunos().add(aluno1);
		turma1.getAlunos().add(aluno2);
		turma1.getAlunos().add(aluno3);
		turma1.getAlunos().add(aluno4);
		turma1.getAlunos().add(aluno5);
		turma1.getAlunos().add(aluno2);
		turma1.getAlunos().add(aluno5);

		System.out.println("Os alunos da turma são:");
		turma1.getAlunos().forEach(System.out::println);
		// Quantos alunos devem ser listados?
		
	}

}
