package br.ucsal.bes20211.poo.aula14.maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class ExemploMapas {

	private static final int QTD_NUM = 10;

	private static final Scanner SCANNER = new Scanner(System.in);

	/*
	 * Crie um programa em Java que solicite do usuário 10 números. Após os 10
	 * números serem informados, o seu programa deverá listar os números distintos e
	 * a quantidade de vezes que os mesmos foram informados.
	 * 
	 * Exemplo:
	 * 
	 * 1, 4, 8, 1, 1, 7, 8, 4, 4, 1
	 * 
	 * 1 x 4
	 * 
	 * 4 x 3
	 * 
	 * 8 x 2
	 * 
	 * 7 x 1
	 */

	public static void main(String[] args) {

		// obterExibirNumerosQtdVetores(QTD_NUM);

		// obterExibirNumerosQtdListas(QTD_NUM);

		// obterExibirNumerosQtdMapas(QTD_NUM);

		obterExibirNumerosQtdMapasJava8(QTD_NUM);
	}

	private static void obterExibirNumerosQtdMapasJava8(int qtdNum) {
		// Map<key, value>
		// Map<número, quantidade>
		Map<Integer, Integer> numsQtds = new HashMap<>();
		System.out.println("Informe " + qtdNum + " números:");
		for (int i = 0; i < qtdNum; i++) {
			int n = SCANNER.nextInt();
			numsQtds.putIfAbsent(n, 0);
			int qtdAtual = numsQtds.get(n);
			numsQtds.put(n, qtdAtual + 1);
		}
		System.out.println("Números distintos = qtd ocorrências:");
		System.out.println(numsQtds);
	}

	private static void obterExibirNumerosQtdMapas(int qtdNum) {
		// Map<key, value>
		// Map<número, quantidade>
		Map<Integer, Integer> numsQtds = new HashMap<>();
		System.out.println("Informe " + qtdNum + " números:");
		for (int i = 0; i < qtdNum; i++) {
			int n = SCANNER.nextInt();
			if (numsQtds.containsKey(n)) {
				int qtdAtual = numsQtds.get(n);
				numsQtds.put(n, qtdAtual + 1);
			} else {
				numsQtds.put(n, 1);
			}
		}
		System.out.println("Números distintos x qtd ocorrências:");
		for (Entry<Integer, Integer> item : numsQtds.entrySet()) {
			System.out.println(item.getKey() + " x " + item.getValue());
		}
	}

	private static void obterExibirNumerosQtdVetores(int qtdNum) {
		int[] nums = new int[qtdNum];
		int[] qtds = new int[qtdNum];
		int qtd = 0;

		System.out.println("Informe " + qtdNum + " números:");
		for (int i = 0; i < qtdNum; i++) {
			int n = SCANNER.nextInt();
			int pos = obterPosicao(n, nums, qtd);
			if (pos >= 0) {
				// o número já está no vetor!
				qtds[pos]++;
			} else {
				// o número NÃO está no vetor!
				nums[qtd] = n;
				qtds[qtd] = 1;
				qtd++;
			}
		}

		System.out.println("Números distintos x qtd ocorrências:");
		for (int i = 0; i < qtd; i++) {
			System.out.println(nums[i] + " x " + qtds[i]);
		}
	}

	/**
	 * Retornar o índice de n dentro do vetor nums. Caso n não esteja em nums,
	 * retorna -1.
	 * 
	 * @param n    o número que está sendo pesquisado
	 * @param nums os números onde será localizado n
	 * @param qtd  a quantidade de posições válidas dentro do vetor nums
	 * @return
	 */
	private static int obterPosicao(int n, int[] nums, int qtd) {
		for (int i = 0; i < qtd; i++) {
			if (n == nums[i]) {
				return i;
			}
		}
		return -1;
	}

	private static void obterExibirNumerosQtdListas(int qtdNum) {
		List<Integer> nums = new ArrayList<>();
		List<Integer> qtds = new ArrayList<>();

		System.out.println("Informe " + qtdNum + " números:");
		for (int i = 0; i < qtdNum; i++) {
			int n = SCANNER.nextInt();
			int pos = nums.indexOf(n);
			if (pos >= 0) {
				// o número já está no vetor!
				int qtdAtual = qtds.get(pos);
				qtds.set(pos, qtdAtual + 1);
			} else {
				// o número NÃO está no vetor!
				nums.add(n);
				qtds.add(1);
			}
		}

		System.out.println("Números distintos x qtd ocorrências:");
		for (int i = 0; i < nums.size(); i++) {
			System.out.println(nums.get(i) + " x " + qtds.get(i));
		}
	}

}
